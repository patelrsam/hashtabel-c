/**********************
* Name: Shubham Patel *
* UID#: 113891779     *
* UID:  spatel95      *
***********************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "htable.h"

/*PROTOTYPES*/

static void free_buckets();
static Bucket* find();
unsigned int hash_code();

/*---------------------------------------------------------------------------*/
			/*
				initializes the table by doing the following:
					-> allocating a bucket array and setting 
					   the pointers to NULL
					-> setting the word count to 0
					-> point to the free_value passed in
					-> setting the correct table_size
			*/																				
/*---------------------------------------------------------------------------*/
int create_table(Table **table, int table_size, void (*free_value)(void *)) {

	if (table != NULL && table_size > 0) {
		int i;

		(*table) = malloc(sizeof(Table));

		if (*table == NULL) {
			return FAILURE;
		}

		(*table)->buckets = calloc(table_size, sizeof(Bucket));

		if ((*table)->buckets == NULL) {
			return FAILURE;
		}

		for (i = 0; i < table_size; i++) {

			(*table)->buckets[i] = NULL;
		}

		(*table)->key_count = 0;
		(*table)->free_value = free_value;
		(*table)->table_size = table_size;

		return SUCCESS;
	}

	return FAILURE;
}

/*---------------------------------------------------------------------------*/
				/*
					Deallocates memory by doing the following:
						-> Freeing the individual buckets
						   in each list
					 	-> freeing the buckets array
					 	-> setting free_value to NULL
					 	-> setting the word count and 
					 	   key_count to 0
					 	-> then finally freeing the table	
				*/
/*---------------------------------------------------------------------------*/


int destroy_table(Table *table) {
	if (table != NULL) {

		free_buckets(table);
		free(table->buckets);
		table->buckets = NULL;
		table->free_value = NULL;
		table->key_count = 0;
		table->table_size = 0;
		free(table);
		table = NULL;

		return SUCCESS;
	}

	return FAILURE;
}

/*---------------------------------------------------------------------------*/
			/*
				Adds key/value data pair to the associated bucket.
				The pair is added to the from of the list. 
			*/
/*---------------------------------------------------------------------------*/

int put(Table *table, const char *key, void *value) {

	if (table != NULL && key != NULL) {

		int idx = hash_code(key) % table->table_size;
		Bucket*prv;
		Bucket *in = find(table, key, &prv);

		prv = in;

		if ( in == NULL) {

			in = malloc(sizeof(Bucket));

			if (in == NULL) {
				return FAILURE;
			}

			in->key = calloc(strlen(key) + 1, 1);

			if (in->key == NULL) {
				return FAILURE;
			}

			strcpy(in->key, key);
			in->value = value;
			in->next = table->buckets[idx];
			table->buckets[idx] = in;

			table->key_count++;
			return SUCCESS;
		} 

		else {

			table->free_value(in->value);
			in->value = value;

			return SUCCESS;
		}

	}
	return FAILURE;
}

/*---------------------------------------------------------------------------*/
		/*
			Sets the in pointer value to the associated value of 
			the data pair
		*/
/*---------------------------------------------------------------------------*/

int get_value(const Table *table, const char *key, void **value) {
	if (table != NULL && key != NULL) {
		
		Bucket *prv;
		Bucket *found = find(table, key, &prv);

		if (found == NULL) {

			(*value) = NULL;
			return FAILURE;
		} else {
			(*value) = found->value;
			return SUCCESS;
		}
	}
	return FAILURE;
}

/*---------------------------------------------------------------------------*/
				/*
					Returns the key count of the table
				*/
/*---------------------------------------------------------------------------*/

int get_key_count(const Table *table) {
	if (table != NULL) {
		return table->key_count;
	}
	return FAILURE;
}

/*---------------------------------------------------------------------------*/
	/*
		Finds and removes the bucket associated with the key
			-> if it is the first it sets the first->next 
			   as first and frees the bucket
			-> if it is not the first, it will set prv->next 
			   as current->next and free current bucket
	*/
/*---------------------------------------------------------------------------*/

int remove_entry(Table *table, const char *key) {
	if (table != NULL && key != NULL &&
	        table->buckets[hash_code(key) % table->table_size]->key != NULL) {

		if (strcmp(table->buckets[hash_code(key) % table->table_size]->key, 
			        key) == 0) {

			Bucket *prv;
			Bucket *first = find(table, key, &prv);

			if (first != NULL) {

				first = table->buckets[hash_code(key) % table->table_size];

				free(first->key);
				first->key = NULL;

				if (first->value != NULL && table->free_value != NULL) {
					table->free_value(first->value);
					first->value = NULL;
				}

				table->buckets[hash_code(key) % table->table_size] = first->next;
				free(first);
				first = NULL;
				
				table->key_count--;
				return SUCCESS;
			}
			return FAILURE;
		}

		else {
			Bucket *prv ;
			Bucket *found = find (table, key, &prv);

			if (found != NULL) {

				free(found->key);
				found->key = NULL;

				if (found->value != NULL && table->free_value != NULL) {
					table->free_value(found->value);
					found->value = NULL;
				}

				prv->next = found->next;
				found->next = NULL;
				
				free(found);
				found = NULL;
				
				table->key_count--;
				return SUCCESS;
			}
		}
		return FAILURE;
	}
	return FAILURE;
}

/*---------------------------------------------------------------------------*/
		/*
			Frees and clears all of the bucket lists from the array
		*/
/*---------------------------------------------------------------------------*/

int clear_table(Table *table) {
	if (table) {
		free_buckets(table);
		table->key_count = 0;
		return SUCCESS;
	}
	return FAILURE;
}

/*---------------------------------------------------------------------------*/
				/*
					checks of the table has no entries in it
				*/
/*---------------------------------------------------------------------------*/

int is_empty(const Table *table) {
	if (table == NULL || table->key_count == 0 ) {
		return SUCCESS;
	}
	return 0;
}

/*---------------------------------------------------------------------------*/
		/*
			Returns the size of the table
		*/
/*---------------------------------------------------------------------------*/

int get_table_size(const Table *table) {
	if (table != NULL) {
		return table->table_size;
	}
	return FAILURE;
}

/*
 * Do not modify this hash_code function.
 * Leave this function at the end of the file.
 * Do not add a prototype to the htable.h file
 * for this function.
 *
 */

unsigned int hash_code(const char *str) {
	unsigned int code;

	for (code = 0; *str; str++) {
		code ^= ((code << 3) | (code >> 29)) + (*str);
	}

	return code;
}



/*------------------------------------------------------------------*/
/*                          STATIC METHODS                          */
/*------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
			/*
				Goes through the Bucket array and then into 
				each of the list, here it frees individual 
				buckets and the data that it holds   
			*/
/*---------------------------------------------------------------------------*/


static void free_buckets(Table *t) {
	int i;

	for (i = 0; i < t->table_size; i++) {
		Bucket *current = t->buckets[i];
		Bucket *temp = NULL;

		if (current != NULL) {

			while (current != NULL) {
				temp = current->next;

				free(current->key);
				current->key = NULL;

				if (current->value != NULL && t->free_value != NULL) {
					t->free_value((void*)current->value);
					current->value = NULL;
				}

				t->buckets[i] = current->next;

				free(current);
				current = NULL;

				current = temp;
			
				temp = NULL;

			}

		} else {
			free(current);
			current = NULL;
		}
	}
}

/*---------------------------------------------------------------------------*/
	/*
		Returns the pointer the bucket associated with the key
		additionally it modifies the in pointer prv to point to
		the previous bucket. 
	*/
/*---------------------------------------------------------------------------*/

static Bucket* find(Table *t, const char *key, Bucket **prv) {
	int idx = hash_code(key) % t->table_size;
	Bucket *current = t->buckets[idx];
	(*prv) = current;

	while (current != NULL) {
		if (strcmp(current->key, key) == 0) {
			return current;
		}

		(*prv) = current;
		current = current->next;
	}
	return current;
}
