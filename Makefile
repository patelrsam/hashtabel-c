CC = gcc
CFLAGS = -ansi -Wall -g -O0 -Wwrite-strings -Wshadow -pedantic-errors -fstack-protector-all 

PROGS = public01 public02 public03 student_tests 


.PHONY: all clean


all: public01 public02 public03 student_tests 

clean:
	rm -f *.o $(PROGS)


public01: public01.o htable.o my_memory_checker_216.o
	$(CC) -o public01 public01.o htable.o my_memory_checker_216.o
public02: public02.o htable.o my_memory_checker_216.o
	$(CC) -o public02 public02.o htable.o my_memory_checker_216.o
public03: public03.o htable.o my_memory_checker_216.o
	$(CC) -o public03 public03.o htable.o my_memory_checker_216.o
student_tests: student_tests.o htable.o my_memory_checker_216.o
	$(CC) -o student_tests student_tests.o htable.o my_memory_checker_216.o


htable.o: htable.c htable.h
	$(CC) $(CFLAGS) -c htable.c
my_memory_checker_216.o: my_memory_checker_216.c my_memory_checker_216.h
	$(CC) $(CFLAGS) -c my_memory_checker_216.c
public1.o: public1.c htable.h my_memory_checker_216.h
	$(CC) $(CFLAGS) -c public01.c 
public2.o: public2.c htable.h my_memory_checker_216.h
	$(CC) $(CFLAGS) -c public02.c 
public3.o: public3.c htable.h my_memory_checker_216.h
	$(CC) $(CFLAGS) -c public03.c 
student_tests.o: htable.h my_memory_checker_216.h
	$(CC) $(CFLAGS) -c student_tests.c

