#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "htable.h"
#include "my_memory_checker_216.h"

/* Set to 0 to remove memory tool function calls */
#define INCLUDE_216_MEMORY_TOOL 1

typedef struct whale {
   char *name;
   int *weight;
} Whale;

static Whale *create_whale(const char *name, int weight);
static void destroy_whale(void *whale_in);
static void print_whale(Whale *whale);

int main() {
   Table *table_ptr;
   int table_size = 5;
   void *value;

   /***** Starting memory checking *****/
   #if INCLUDE_216_MEMORY_TOOL
   start_memory_check();
   #endif
   /***** Starting memory checking *****/

   /* Notice we are providing a destroy_whale function */
   /* (a free_value function). The hash table has no   */
   /* idea that we are storing whales so it cannot     */
   /* make copies of them and can only free the        */
   /* dynamically-allocated memory associated with the */
   /* whale using the destroy_whale function provided. */

   /* Creating the table */
   create_table(&table_ptr, table_size, destroy_whale);

   /* Inserting the whale */
   put(table_ptr, "Willy", create_whale("Willy", 3000));
   put(table_ptr, "a", create_whale("a", 3000));
   put(table_ptr, "b", create_whale("b", 2000));
   put(table_ptr, "c", create_whale("c", 1));


   /* Retrieving the whale */
   get_value(table_ptr, "Willy", &value);
   remove_entry(table_ptr,"a");
   remove_entry(table_ptr,"c");


   /* Printing the whale */
   print_whale(value);

   /* Destroying the table */
   destroy_table(table_ptr);

   /****** Gathering memory checking info *****/
   #if INCLUDE_216_MEMORY_TOOL
   stop_memory_check();
   #endif
   /****** Gathering memory checking info *****/

   return 0;
}

static Whale *create_whale(const char *name, int weight) {
   Whale *whale = malloc(sizeof(Whale));

   whale->name = malloc(strlen(name) + 1);
   strcpy(whale->name, name);

   whale->weight = malloc(sizeof(int));
   *(whale->weight) = weight;

   return whale;
}

static void destroy_whale(void *whale_in) {
   Whale *whale = whale_in;

   free(whale->name);
   free(whale->weight);
   free(whale);
}

static void print_whale(Whale *whale) {
   printf("Whale's name: %s\n", whale->name);
   printf("Whale's weight: %d\n", *whale->weight);
}
