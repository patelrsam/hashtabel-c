#include <stdio.h>
#include <stdlib.h>
#include "htable.h"
#include "my_memory_checker_216.h"

/*****************************************************/
/* In this file you will provide tests to your       */
/* hash table.  Each test should be named test1()    */
/* test2(), etc. Each test must have a description   */
/* of what it is testing (this description is really */
/* important).                                       */
/*                                                   */
/* You can tell whether any test failed if you       */
/* execute on the command line "echo $?" and you get */
/* a value other than 0. "echo $?" prints the status */
/* of the last command.                              */
/*                                                   */
/* main just calls all test1(), test2(), etc.        */
/*****************************************************/

static void print_list(Bucket *bucket) {
   if (bucket != NULL) {
      if (bucket->value == NULL) {
         printf("\tKey: %s, value: NULL\n", bucket->key);
      }
      else {
         printf("\tKey: %s, value: %d\n", bucket->key, *((int *) bucket->value));
      }
      print_list(bucket->next);
   }
}
static void display_table(Table *table) {
   int i;
   if (table == NULL) {
      printf("TABLE IS NULL\n");
      return;
   }
   printf("Key_count: %d\n", table->key_count);
   printf("Table_size: %d\n", table->table_size);
   if (table->key_count == 0) {
      printf("Empty table\n");
   } else {
      for (i = 0; i < table->table_size; i++) {
         printf("\nBucket index: %d\n", i );
         print_list(table->buckets[i]);
      }
   }
}


/* basic test to make sure gets initialized correctly  */
static int test1() {
   int table_size = 2;
   Table *table_ptr;
   printf("\n***************************TEST1********************************\n");
   printf("--->Creating Table");
   create_table(&table_ptr, table_size, free);
   printf("...DONE\n");

   printf("\n---> Starting to perform tests");

   if (is_empty(table_ptr) != SUCCESS) {
      printf("Test1 -> is_empty\n");
      return FAILURE;
   }

   if (get_key_count(table_ptr) != 0) {
      printf("Test1 -> key_count\n");
      return FAILURE;
   }
   printf("...DONE\n");

   printf("\n---> Destroy Empty Table");
   destroy_table(table_ptr);
   printf("...DONE\n");

   printf("\n***************************TEST1********************************\n");
   return SUCCESS;
}
/*testing of all methods to make sure they perform */
static int test2() {
   int table_size = 5;
   Table *t;

   int *value;

   int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, h = 8, i = 9, j = 10,
       p = 11, s = 12, v = 13, w = 14, y = 15, z = 16, aa = 17;


   printf("\n***************************TEST2********************************\n");
   printf("---> Creating Table");
   create_table(&t, table_size, NULL);
   printf("...DONE\n");

   printf("\n---> Putting items in the table");
   put(t, "a", (void *)&a);
   put(t, "b", (void *)&b);
   put(t, "c", (void *)&c);
   put(t, "d", (void *)&d);
   put(t, "e", (void *)&e);
   put(t, "f", (void *)&f);
   put(t, "g", (void *)&g);
   put(t, "h", (void *)&h);
   put(t, "i", (void *)&i);
   put(t, "j", (void *)&j);
   put(t, "p", (void *)&p);
   put(t, "s", (void *)&s);
   put(t, "v", (void *)&v);
   put(t, "w", (void *)&w);
   put(t, "y", (void *)&y);
   put(t, "z", (void *)&z);
   put(t, "aa", (void *)&aa);
   printf("--->DONE\n");


   display_table(t);
   printf("\n---> Starting to perform tests");

   if (get_key_count(t) != 17) {
      printf("Test 2 FAIL ---- key count\n");
      printf("Expected: 17 but was: %d\n", get_key_count(t));
      return FAILURE;
   }

   if (get_table_size(t) != 5) {
      printf("Test 2 FAIL ---- table size\n");
      printf("Expected: 5 but was: %d\n", get_key_count(t));
      return FAILURE;
   }
   if (is_empty(t) != 0) {
      printf("TEST 2 FAIL ---- is empty (non empty table) \n");
   }

   get_value(t, "a", (void*)&value);
   if (*value != 1) {
      printf("Test 2 FAIL ---- get value: a \n");
      printf("Expected: 1 but was: %d\n", *value);
      return FAILURE;
   }
   get_value(t, "j", (void*)&value);
   if (*value != 10) {
      printf("Test 2 FAIL ---- get value: j \n");
      printf("Expected: 10 but was: %d\n", *value);
      return FAILURE;
   }

   get_value(t, "w", (void*)&value);
   if (*value != 14) {
      printf("Test 2 FAIL ---- get value: w \n");
      printf("Expected: 14 but was: %d\n", *value);
      return FAILURE;
   }

   get_value(t, "c", (void*)&value);
   if (*value != 3) {
      printf("Test 2 FAIL ---- get value: c \n");
      printf("Expected: 3 but was: %d\n", *value);
      return FAILURE;
   }

   get_value(t, "s", (void*)&value);
   if (*value != 12) {
      printf("Test 2 FAIL ---- get value: s \n");
      printf("Expected: 12 but was: %d\n", *value);
      return FAILURE;
   }

   remove_entry(t, "f");
   remove_entry(t, "d");
   remove_entry(t, "z");

   if (get_value(t, "f", (void*)&value) == SUCCESS) {
      printf("Test 2 FAIL ---- get value: (REMOVED) f\n");
   }

   if (get_value(t, "d", (void*)&value) == SUCCESS) {
      printf("Test 2 FAIL ---- get value: (REMOVED) d\n");
   }

   if (get_value(t, "z", (void*)&value) == SUCCESS) {
      printf("Test 2 FAIL ---- get value: (REMOVED) z\n");
   }

   if (get_key_count(t) != 14) {
      printf("Test 2 FAIL---- key count\n");
      printf("Expected: 14 but was: %d\n", get_key_count(t));
   }

   clear_table(t);

   if (get_key_count(t) != 0) {
      printf("Test 2 FAIL ---- key count(AFTER CLEAR)\n");
      printf("Expected: 0 but was: %d\n", get_key_count(t));
   }

   printf("...DONE\n");
   display_table(t);

   printf("\n---> Destroy Empty Table");
   destroy_table(t);
   printf("...DONE\n");

   printf("\n***************************TEST2********************************\n");

   return SUCCESS;
}

static int test3() {
   int table_size = 5;
   int test_int = -1;
   Table *t;
   Table *tst = NULL;
   int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, h = 8, i = 9, j = 10,
       p = 11, s = 12, v = 13, w = 14, y = 15, z = 16;
   int *value;
   printf("\n***************************TEST3********************************\n");
   printf("\n---> Creating Table");
   create_table(&t, table_size, NULL);
   printf("...DONE\n");

   printf("\n---> Puting items in the table");
   put(t, "a", (void *)&a);
   put(t, "b", (void *)&b);
   put(t, "c", (void *)&c);
   put(t, "d", (void *)&d);
   put(t, "e", (void *)&e);
   put(t, "f", (void *)&f);
   put(t, "g", (void *)&g);
   put(t, "h", (void *)&h);
   put(t, "i", (void *)&i);
   put(t, "j", (void *)&j);
   put(t, "p", (void *)&p);
   put(t, "s", (void *)&s);
   put(t, "v", (void *)&v);
   put(t, "w", (void *)&w);
   put(t, "y", (void *)&y);
   put(t, "z", (void *)&z);
   put(t, "aa", NULL);
   printf("...DONE\n");


   printf("\n---> Starting to perform tests");

   if (create_table(NULL, 2, NULL) == SUCCESS) {
      printf("TEST 3 FAIL---- Creat Table (NULL table)\n");
      return FAILURE;
   }
   if (create_table(&tst, 0, NULL) == SUCCESS) {
      printf("TEST 3 FAIL ---- Creat Table (NULL 0 size)\n");
      return FAILURE;
   }


   if (destroy_table(tst) == SUCCESS) {
      printf("TEST 3 FAIL ---- Destroid tst\n");
      return FAILURE;
   }


   value = &test_int;
   if (put(NULL, "xa", (void*)&value) == SUCCESS) {
      printf("TEST 3 FAIL ---- put (NULL table)\n");
      return FAILURE;
   }
   if (put(t, NULL, (void*)&value) == SUCCESS) {
      printf("TEST 3 FAIL ---- put (NULL key)\n");
      return FAILURE;
   }

   if (get_value(NULL, "a", (void*)&value) == SUCCESS) {
      printf("TEST 3 FAIL --- get value (NULL table)\n");
      return FAILURE;
   }
   if (get_value(t, NULL, (void*)&value) == SUCCESS) {
      printf("TEST 3 FAIL --- get value (NULL value)\n");
      return FAILURE;
   }
   if (get_value(t, "xx", (void*)&value) == SUCCESS) {
      printf("TEST 3 FAIL --- get value (SUCCESS on nonexistent key\n");
      return FAILURE;
   }

   get_value(t, "xx", (void*)&value);
   if (value != NULL) {
      printf("TEST 3 FAIL --- get value NULL\n");
      return FAILURE;
   }

   if (get_key_count(NULL) == SUCCESS) {
      printf("TEST 3 FAIL --- get key NULL\n");
      return FAILURE;
   }

   if (remove_entry(NULL, "a") == SUCCESS) {
      printf("TEST 3 FAIL --- remove entry (NULL table)\n");
      return FAILURE;
   }
   if (remove_entry(t, NULL) == SUCCESS) {
      printf("TEST 3 FAIL --- remove entry (NULL key)\n");
      return FAILURE;
   }
   if (remove_entry(t, "xx") == SUCCESS) {
      printf("TEST 3 FAIL --- remove entry (nonexistent key)\n");
      return FAILURE;
   }

   remove_entry(t, "a");
   if (remove_entry(t, "a") == SUCCESS) {
      printf("TEST 3 FAIL --- remove entry (already removed key)\n");
      return FAILURE;
   }

   if (clear_table(NULL) == SUCCESS) {
      printf("TEST 3 FAIL ---- clear table (NULL)\n");
   }
   printf("...DONE\n");


   printf("\n---> Destroy Table");
   destroy_table(t);
   printf("...DONE\n");

   printf("\n***************************TEST3********************************\n");
   return SUCCESS;
}

int main() {
   int result = SUCCESS;

   /***** Starting memory checking *****/
   start_memory_check();
   /***** Starting memory checking *****/

   if (test1() == FAILURE) result = FAILURE;
   if (test2() == FAILURE) result = FAILURE;
   if (test3() == FAILURE) result = FAILURE;

   /****** Gathering memory checking info *****/
   stop_memory_check();
   /****** Gathering memory checking info *****/

   if (result == FAILURE) {
      exit(EXIT_FAILURE);
   }

   printf("\n::::::::::::::::::::::::::::\n");
   printf("           SUCCESS            \n");
   printf("::::::::::::::::::::::::::::\n\n");
   return EXIT_SUCCESS;
}
